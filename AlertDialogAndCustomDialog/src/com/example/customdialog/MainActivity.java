package com.example.customdialog;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener{
	List<String> movieList;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button alertBtn=(Button) findViewById(R.id.alert_dialog_button);
		Button customBtn=(Button) findViewById(R.id.custom_dialog_button);
		// add button listener
		alertBtn.setOnClickListener(this);
		customBtn.setOnClickListener(this);
		movieList=new ArrayList<String>();
		movieList.add("Happy New Year");
		movieList.add("Chennai Express");
		movieList.add("Bahubali");
		movieList.add("Fast and Furious ");
		movieList.add("Fifty Shades Of The Gray");
		movieList.add("Srimanthudu");
		movieList.add("Dhoom 3");
		movieList.add("Krish 3");
		

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		// creating alert dialog
		case R.id.alert_dialog_button:
			// this is the alert dialog 
			AlertDialog.Builder dialogBuilder =new AlertDialog.Builder(MainActivity.this);
			dialogBuilder.setTitle("Alert Dialog ");
		dialogBuilder.setMessage("Click Yes to Exit!");
			dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// if this button is clicked, close
					// current activity
					MainActivity.this.finish();
				Toast.makeText(getApplicationContext(), "clicked on yes button", Toast.LENGTH_SHORT).show();	
				}
			});	
dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// if this button is clicked, just close
					// the dialog box and do nothing
					dialog.cancel();
				Toast.makeText(getApplicationContext(), "clicked on no button", Toast.LENGTH_SHORT).show();	
				}
			});	
//create alert dialog
AlertDialog alertDialog = dialogBuilder.create();

// show it
alertDialog.show();
			break;
			// creating custom dialog 
		case R.id.custom_dialog_button:
			showCustomDialog();
			break;
		default:
			break;
		}
		
	}
	void showCustomDialog(){
		final Dialog customDialog=new Dialog(MainActivity.this);
		 customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	        customDialog.setContentView(R.layout.dialog_template);
	        Button button1 = (Button) customDialog.findViewById(R.id.doneButton);
	       
	        TextView dialogTitle = (TextView) customDialog.findViewById(R.id.dialogTitle);
	        dialogTitle.setText(" Movie Details :");
	       
	        ListView listview = (ListView) customDialog.findViewById(R.id.dialogListView);
	       CustomAdapter  adapter=new CustomAdapter(getApplicationContext(),R.layout.list_item,movieList );
	        listview.setAdapter(adapter);
	       
	        button1.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
				customDialog.cancel();
					
				}
			});
	        listview.setOnItemClickListener(new OnItemClickListener() {

	            @Override
	            public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
	                
	                Toast.makeText(getApplicationContext(), "Clicked on item " +movieList.get(position), Toast.LENGTH_SHORT).show();
	            }
	        });
	        customDialog.show();
	}

}
